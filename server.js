var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app);

// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set('view engine', 'ejs');
// End of view engine setup

// manage our entries
var entries = [];
app.locals.entries = entries;


// set up the logger 
//only log error responses
app.use(logger("dev"));
/*app.use(logger('combined',{
  skip: function (req, res) { return res.statusCode < 400 }
}));*/
// End of logger
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname+'/assets/')));
// GETS
app.get("/", function(req,res){
	res.sendFile(path.join(__dirname+'/assets/index.html'));
});

app.get("/about", function(req,res){
  res.sendFile(path.join(__dirname+'/assets/about.html'));
});

app.get("/contact", function(req,res){
  res.sendFile(path.join(__dirname+'/assets/contact.html'));
});

app.get("/services", function(req,res){
  res.sendFile(path.join(__dirname+'/assets/services.html'));
});

app.get("/new-entry", function(req,res){
  res.render("new-entry");
}); 

app.post("/new-entry",function(req,res){
  entries.push({
    author: req.body.author,
    title: req.body.title,
    content: req.body.body,
    published: new Date()
  });
  res.redirect("/guestbook");
});

app.get("/guestbook", function(req,res){
	res.render("guestbook");
});
// ENd of GET request handling

// POSTS
app.post('/contact',function(req,res){
   if (!req.body.nm1 || !req.body.email || !req.body.message) {
    res.status(400).send("Entries must have a title and a body.");
    return;
  }
  var api_key = 'key-70e81fbcdc36f7eae694e80d05fd6430';
  var domain = 'sandbox32e4a10b35f94f7591bd12e7394c30e2.mailgun.org';
  var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
   
  var data = {
    from: 'Ravva Vamsi <postmaster@sandbox32e4a10b35f94f7591bd12e7394c30e2.mailgun.org>',
    to: 'vamsi.ravva@gmail.com',
    subject: req.body.nm1+" sent you a message",
    html: "<b>Email : </b>"+ req.body.email + "<br/><b>Message : </b>"+ req.body.message
  };
   
  mailgun.messages().send(data, function (error, body) {
    console.log(body);
    res.send("Mail Sent");
  });
});

// 404
app.use(function(req, res, next){
  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    res.render('404', { url: req.url });
    return;
  }

  // respond with json
  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  // default to plain-text. send()
  res.type('txt').send('Not found');
});	
//End of 404 error handling

// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('A03RavvaVamsi app listening on http://127.0.0.1:8081/');
});
