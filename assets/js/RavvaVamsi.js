
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function priceCal() {
	var products = $("[name='check']");
	var ttlPrice = 0;
	for( var i=0; i<products.length;i++){
		if(products[i].checked){
				if(i==0)
					ttlPrice = sum(ttlPrice, multiply(parseInt($("[name='hostingType']:checked").val()), parseInt($("#qty-"+(i+1)).val()))*12);
					// ttlPrice += parseInt($("[name='hostingType']:checked").val())* parseInt($("#qty-"+(i+1)).val());
				else if(i<4)	
					ttlPrice = sum(ttlPrice, multiply(parseInt(products[i].value), parseInt($("#qty-"+(i+1)).val())));
			}
	}
	var ttP = document.getElementsByClassName("ttlAmnt");
	for(var j=0;j<ttP.length;j++)
		ttP[j].innerHTML = ttlPrice+"$";
}
	
function sum(a,b){
	if(!isNaN(a) && !isNaN(b))
		return a+b;
	else
		throw Error("only numbers are allowed");
}
function multiply(a,b){
	if(!isNaN(a) && !isNaN(b))
		return a*b;
	else
		throw Error("only numbers are allowed");
}

$("#contactSbmt").click(function(evt){
	evt.preventDefault();
	$("#contactSbmt").html("Mail Sending <img src='images/progress.gif' width='50px' />  ");
	$.ajax({
	  type: "POST",
	  url:  "contact",
	  data: $("#contactForm").serialize(),
	  success: function(success){
	  			alert(success);
				$("#contactSbmt").html("Submit");  
	  		}
	});
});